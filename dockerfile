FROM node:20-alpine as build

COPY . .
RUN npm i && npm run build && mkdir application
RUN mv .output/ application/

FROM node:20-alpine

COPY --from=build application/.output /
